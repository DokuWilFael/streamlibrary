# Problématique

Je suis l'heureux propriétaire d'un clavier Logitech G710+. Ce clavier dispose de deux rangées de touches supplémentaires (G1-G6 et M1-M3-MR).
Sous Windows, avec le logiciel de gestion de Logitech, ces touches permettent d'enregistrer des macros au niveau du clavier et de naviguer
entre différents profils de macros.

Sous Linux, ces fonctionnalités n'existent pas car elles sont dépendantes du logiciel de gestion.
Par défaut, les touches M ne font rien et les touches G sont des copies des touches 1 à 6 présentes sur le clavier (celle qui font les symboles, pas le pavé numérique).

En mettant en place mes Overlays OBS, j'ai ressenti le besoin d'avoir des raccourcis claviers supplémentaires et notamment pas sur Ctrl+Alt+touche du pif.
J'ai donc cherché à faire marcher les touches supplémentaires que j'avais déjà sous la main.

Accrovhez-vous, c'est pas une mince affaire

# Drivers

Première étape, trouver un driver qui marche. Après quelques recherches, on fini par tomber sur le [wiki Arch](https://wiki.archlinux.org/index.php/Logitech_Gaming_Keyboards)
et sur le [repo git de Wattos](https://github.com/Wattos/logitech-g710-linux-driver). C'est la solution que j'ai choisi car elle semblait la plus simple
et la plus stable. Oui, il y a des forks plus récents mais j'ai préféré rester sur la source que j'ai vu conseillé à plusieurs endroits.

Après compilation et installation du driver (suivre la documation sur git, attention au dépendances de dev type linux-headers et tout le bazar), on peux redémarrer la machine.
Miracle, les touches font en effet des choses mais pour des raisons de compatibilités pour les pc portables, les codes utilisés (F13 à F23)
sont mappés sur des actions existantes type allumer/éteindre le touchpad.

Je suis presque sûr qu'il va falloir ré-installer le driver à la main à la moindre mise à jour de noyau mais je n'ai pas encore eu à en faire entre
la mise en place et l'écriture de ce tuto/pense-bête. De même, le driver propose une interface pour contrôler le rétro-éclairage du clavier, pour l'instant je n'ai pas essayé
d'en faire quoi que ce soit.

# Remapping clavier

On attaque le gros morceaux. Comme vu au dessus, le mapping clavier est étrange. Problème, changer le mapping des touches
est une jungle sans fond sous Linux. Il y a des millions de questions de ce type sur tout les forums/StackExchange/askubuntu/etc.
Bien entendu, il y a donc des millions de réponses différentes, qui ne marchent pas/ ne sont pas complètes, bref, comme je le disais, la jungle.

Après plusieurs heures de recherches et de tests, j'ai fini par trouver ce tuto (en anglais) : [A simple, humble but comprehensive guide to XKB for linux](https://medium.com/@damko/a-simple-humble-but-comprehensive-guide-to-xkb-for-linux-6f1ad5e13450). Il explique très bien les différences entre xmodmap et xkb, le fonctionnement de ce dernier et comment accéder
aux informations de son clavier.

Je ne vais pas entrer ici dans les détails du bazar mais pour faire simple xmodmap applique des changements à la volée à la configuration qui sont succeptibles d'être écraser à n'importe quel moment.
Et quand je dis à n'importe quel moment, c'est vraiment le cas. Les modifications ne survivent pas le changement de langue si vous souhaitez jouer à un jeu qui ne supportent qu'un clavier qwerty,
peuvent disparaitre à la mise en veille, au passage au noir de l'écran, à la fermeture de la session, si un autre programme viens regarder le configuration clavier. Bref, ça marche ponctuellement mais
c'est un enfer à rendre persistent. Et le comportement est dépendant de votre choix d'environnement de bureau également (Gnome chez moi, connu pour ne pas être sympa avec xmodmap).

Nous sommes donc obligé de nous tourner vers xkb et son architecture complexe. La encore, je ne vais pas rentrer dans les détails car je n'ai pas tout compris loin de là.
Des différentes façon de faire (fichier de config au démarrage de X11, modification de fichier par défaut existant,etc...), j'ai choisi d'éditer directement les fichiers de par défaut,
pour des questions de simplicité de mise en place et de persistence.

En utilisant la commande ```setxkbmap -print -verbose 10```, on a la résultat suivant sur mon système :
```
Setting verbose level to 10
locale is C
Trying to load rules file ./rules/evdev...
Trying to load rules file /usr/share/X11/xkb/rules/evdev...
Success.
Applied rules from evdev:
rules:      evdev
model:      pc105
layout:     fr,us,fr
variant:    oss,,oss
options:    lv3:ralt_switch
Trying to build keymap using the following components:
keycodes:   evdev+aliases(azerty)
types:      complete
compat:     complete
symbols:    pc+fr(oss)+us:2+fr(oss):3+inet(evdev)+level3(ralt_switch)
geometry:   pc(pc105)
xkb_keymap {
	xkb_keycodes  { include "evdev+aliases(azerty)"	};
	xkb_types     { include "complete"	};
	xkb_compat    { include "complete"	};
	xkb_symbols   { include "pc+fr(oss)+us:2+fr(oss):3+inet(evdev)+level3(ralt_switch)"	};
	xkb_geometry  { include "pc(pc105)"	};
};
```
On observe que les keycodes utilisé contiennent evdev. C'est ce fichier qui défini les appuies touches (codes numériques types 193,194,etc...) en FK13-FK23 (Fonction Key sans doute).
On remarque aussi que le fichier de symbole associé à evdev est inet et en effet en regardant sa structure, c'est bien lui qui transforme FK13-FK23 en symbole type "XF86TrucMachin".
J'ai donc modifié dans la catégorie evdev les touches FK13-FK23 pour qu'elles renvoient bien les symboles F13-F23 et un reboot plus tard, tout marche.

Un extrait de mon fichier /usr/share/X11/skb/symbols/inet pour référence :
```
// Evdev Standardized Keycodes
partial alphanumeric_keys
xkb_symbols "evdev" {
    key <MUTE>   {      [ XF86AudioMute         ]       };
    key <VOL->   {      [ XF86AudioLowerVolume  ]       };
    key <VOL+>   {      [ XF86AudioRaiseVolume  ]       };
[...]
    key <FK21>   {      [ F21, F21, F21, F21    ]       };
    key <FK22>   {      [ F22, F22, F22, F22    ]       };
    key <FK23>   {      [ F23, F23, F23, F23    ]       };

    key <FK20>   {      [ F20, F20, F20, F20    ]       };
[...]
    key <I372>  {       [ XF86Favorites          ]       };
    key <I382>  {       [ XF86Keyboard           ]       };
    key <I569>  {       [ XF86RotationLockToggle ]       };
    key <I380>  {       [ XF86FullScreen         ]       };

    key <FK13>   {      [ F13, F13, F13, F13     ]       };
    key <FK14>   {      [ F14, F14, F14, F14     ]       };
    key <FK15>   {      [ F15, F15, F15, F15     ]       };
    key <FK16>   {      [ F16, F16, F16, F16     ]       };
    key <FK17>   {      [ F17, F17, F17, F17     ]       };
    key <FK18>   {      [ F18, F18, F18, F18     ]       };
    key <FK19>   {      [ F19, F19, F19, F19     ]       };
};
```


Je peux maintenant utiliser les touches G1-G6/M1-M3+MR comme touches de fonctions numérotés 13 à 23 et les assigner sur des raccourcis dans OBS ou n'importe ou ailleurs.

# License
Ce texte est mis à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International par Doku Wil'Faël
