 # Problématique


Par défaut, OBS écoute la sortie audio par défaut du système pour son entrée audio "Audio de bureau".
Quand l'on rajoute une source audio dans OBS, il est possible de la monitorer (aka, de l'écouter en direct). Cette option est là pour permettre à l'utilisateur de surveiller le volume de la source notamment.
Or, pour permettre à l'utilisateut d'entendre les sources, par défaut, OBS envoi le signal sur la sortie par défaut du système.
Résultat, si vous diffusez l'audio de bureau + une autre source audio que vous monitorez, le résultat final présentera de l'écho/distorsion car la source audio monitoré est présente 2 fois (elle-même + le moniteur qui repasse par l'audio de bureau).

# Solutions


Pour pallier à ce problème, il faut donc régler OBS pour que l'audio de bureau et le monitoring n'utilise pas les mêmes sorties.

* Si la machine utilisé propose plusieurs sorties (audio via HDMI par exemple), on peux écouter sur OBS (Audio de bureau) l'une de ses sorties non utilisées (S1) puis utiliser le monitoring pour écouter le son de l'Audio de Bureau. Il faut ensuite régler dans les paramètres sons (via pavu-control) chaque application pour qu'elle envoie le son sur la sortie S1. On règle le monitor d'OBS pour utiliser la sortie S2, correspondant à la sortie par défaut de notre machine, celle que l'on écoute habituellement.
Le problème avec cette méthode c'est que le moniteur d'OBS, de mon expérience, introduit une latence énorme dans la chaîne audio (entre 2 et 5 secondes). Cela rend donc cette méthode non utilisable pour tout types de son que l'on voudrait en "temps réel" (conversation avec d'autres intervenants, son de jeux, etc...). De plus, si OBS n'est pas allumé, vous n'entendrez pas vos applications car le son sortira sur la sortie S1.

* Au lieu d'utiliser le monitoring d'OBS pour récupérer le son du bureau, on utilise directement Pulse Audio en créant un module de type loopback. Cela permet d'avoir un "moniteur" de la sortie S1 qui est écoutable via la sortie S2. Il faut toujours régler manuellement les applications sur pavu-control mais plus besoin d'avoir OBS allumé pour entendre le son, Pulse Audio s'en occupe.
De plus, si vous n'avez qu'une seule sortie audio sur votre machine, Pulse Audio permet de créer une sortie virtuelle que vous pourrez également "loopback" sur votre sortie physique.
Enfin, la latence rajouté par le loopback est de l'ordre de 50ms donc rien de très perceptible par rapport à celle d'OBS.

# Configuration


Il faut donc configurer Pulse Audio pour créer une sortie virtuelle + un loopback de cette sortie vers la sortie physique. Il faut donc executer le code suivant dans un bash utilisateur
```
pacmd load-module module-null-sink sink_name=VirtualSpeaker1 sink_properties=device.description=VirtualSpeaker1
pacmd load-module module-loopback source=VirtualSpeaker1.monitor
```

Cela va créer une sortie virtuelle nommé VirtualSpeaker1 et créer le loopback vers la sortie par défaut de votre système.
Malheureusement, ces changements ne sont que temporaires et seront supprimer a l'extinction de la machine. Pour les rendre persistents, 2 solutions

## 1

Rajouter les lignes au fichier /etc/pulse/default.pa

``` sudo nano /etc/pulse/default.pa ```

Rajouter les lignes suivantes au bout du fichier
```
load-module module-null-sink sink_name=VirtualSpeaker1 sink_properties=device.description=VirtualSpeaker1
load-module module-loopback source=VirtualSpeaker1.monitor
 ```

## 2
Mettre en place un script qui s'exécute au démarrage de la machine/session pour automatiser la création de la sortie virtuelle et de son loopback. C'est la solution que j'ai choisi car bien que moins élégante, elle ne necessite pas d'aller modifier des fichiers systèmes par défaut et est uniquement côté utilisateur.

Mise en place
1- Créer un fichier de script avec les deux lignes de commandes et le rendre exécutable (ici pavu_loopbacks.sh)
2- Ajoutez un fichier votrenom.desktop dans ~/.config/autostart avec le contenu suivant :
```
[Desktop Entry]
Name=Pulse Audio Virtual loopbacks
GenericName=Pulse Audio Virtual loopbacks
Comment=Recreating pulse audio virtual sinks and loopbacks at startup
Exec=/home/doku/pavu_loopbacks.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true
```

# License


Ce texte est mis à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International par Doku Wil'Faël
