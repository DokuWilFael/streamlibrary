# Problématique

J'utilise X11 en temps que serveur graphique car Wayland n'est pas encore suffisement supporté par Wine/Proton.
Le problème est l'apparition de "Screen Tearing", c'est à dire de désynchronisation verticale entre ce que sort ma carte graphique et ce qu'affiche mon écran.
Cela résulte dans l'apparition d'artefact type bande horizontale qui coupe l'écran et c'est moche

# Solution

Ici très simple, après installation du bon driver x11, pour moi ```xorg-x11-drv-ampgpu```, il suffit de passer l'option TearFree sur on pour tout les écrans.
Pour ce faire, j'utilise les commandes suivantes :
```
xrandr --output HDMI-A-0 --set TearFree on
xrandr --output DisplayPort-1 --set TearFree on
xrandr --output DisplayPort-2 --set TearFree on
xrandr --output DisplayPort-0 --set TearFree on
```
Les changements effectué avec xrandr ne sont malheuresement pas persistent. Il faut donc scripter l'exécution de ses commandes à l'ouverture de session gnome
pour que la modification soit appliqué automatiquement à chaque démarrage.

Pour ce faire, on créé un fichier de script avec les commandes et on le rend exécutable (ici tear_free.sh). Ensuite, on rajoute un .desktop dans ~/.config.autostart avec le contenu suivant:
```
[Desktop Entry]
Name=TearFree
GenericName=TearFree option
Comment=Needed to manually set tear free flag so automating it a little
Exec=/home/doku/tear_free.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true
```
Et voilà, plus de problèmes de tearing. Attention, cette configuration marche pour ma machine et n'a pas été testé ailleurs.
Je vous invite à rechercher comment faire sur le net en fonction du constructeur de votre carte graphique (Intel, NVidia, AMD) et des drivers que vous avez installé (ampgpu, mesa, nouveau, akmod-nvidia, etc...).

# License


Ce texte est mis à disposition selon les termes de la Licence Creative Commons Attribution 4.0 International par Doku Wil'Faël
