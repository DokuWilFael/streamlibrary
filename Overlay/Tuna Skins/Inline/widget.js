var last_cover = '';
        var last_artist = '';
        var last_title = '';

        function format_ms(s) {
            var ms = s % 1000;
            s = (s - ms) / 1000;
            var secs = s % 60;
            s = (s - secs) / 60;
            var mins = s % 60;
            var hrs = (s - mins) / 60;

            if (secs < 10)
                secs = '0' + secs;
            
            if (hrs > 0) {
                if (mins < 10)
                    mins = '0' + mins;
                return hrs + ':' + mins + ':' + secs;
            }
            return mins + ':' + secs;
        }

        function fetch_data() {
            fetch('http://localhost:1608/')
            .then(response => response.json())
            .then(data => {
                // data now contains the json object with song metadata

                
                // artist list
                var artists = '';
                var array = data['artists'];
                for (var i = 0; i < array.length; i++) {
                    artists += array[i];
                    if (i < array.length - 1)
                        artists += ', ';
                }

                document.getElementById('infos').innerText = data['title'];
                if (data['cover_url'] !== last_cover || // Refresh only if meta data suggests that the cover changed
                    (data['title'] !== last_title &&    // When using MPD the path is always the cover path configured in tuna
                    artists !== last_artist))           // which means it won't change so we check against other data
                {
                    // Random number at the end is to prevent caching
                    document.getElementById('cover').src = data['cover_url'] + '?' + Math.random();
                    last_cover = data['cover_url'];
                }

                if (artists === data['album'] || data['album'] === undefined) // Some singles have the artist as the album, which looks strange with the other subtitle
                    document.getElementById('infos').innerText += " - "+ artists;
                else
                    document.getElementById('infos').innerText += " - "+ artists
                
                last_artist = artists;
                last_title = data['title'];
            })
            .catch(function() {
                // Do nothing
            });
        }
        
        setInterval(fetch_data, 500);
