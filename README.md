# Stream Library

Ce projet centralise tout les assets (graphiques, textuels ou autres) et outils que j'utilise ou développe pour mon activité de Streaming.

Les licenses utilisées ici varient en fonction du type de contenu et de leurs licenses originales (le cas échéant).

Ce projet utilise git lfs pour gérer les gros fichiers, tel que les images. Pensez à installer et à activer git lfs si vous le clonez.
